Only Admin Login
URL: http://drupal.org/sandbox/sergio.durzu/1277600
===============================


DESCRIPTION
-----------
This module simply disables login (from login boxes) for all users except admin.
Useful if you use external services (OpenID, Salm based services) to login users 
but you want also to keep a way to login into the system (what happens if the 
external login service goes down?).
This module also prevent the password-recovery function.


REQUIREMENTS
------------
Drupal 6.x


INSTALLING
----------
Copy the directory only_admin_login in your module's directory and enabled it. 

INFO 
----
This module disables login only from login box, beacuse the login function must works
for external service.
So you have, if you want, to remove all login box.
But if a user go to site.com/user and try to login, the system stops it.

INFO
----
Module created by Sergio Durzu - sergio.durzu@ildeposito.org


